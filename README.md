## User Prune

User Prune lets you mass delete inactive users based on criteria you specify
on every cron job.

The main criteria is the time since the user last logged in or, if never
logged in before, the time since the user was created.

Optionally, some criteria can be set to Prune users:

* by status (blocked or active)
* who never posted comments
* who never logged in

### Requirements

No special requirements, except for the Drupal core user module.

### Install

Install module via composer:

```bash
composer require drupal/user_prune
drush en user_prune
```

### Configuration

Once module is installed and enabled, please see configuration page:
**/admin/structure/user-prune/settings**

Configure as needed. Run cron to automatically prune users.

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
