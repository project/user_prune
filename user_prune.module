<?php

/**
 * @file
 * Provides the main functionality of the module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function user_prune_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'help.user_prune') {
    $text = file_get_contents(__DIR__ . '/README.md');
    if (!Drupal::moduleHandler()->moduleExists('markdown')) {
      return '<pre>' . $text . '</pre>';
    }
    else {
      /** @var \Drupal\markdown\PluginManager\ParserManagerInterface $parser_manager */
      $parser_manager = \Drupal::service('plugin.manager.markdown.parser');
      $parser = $parser_manager->getDefaultParser([
        'render_strategy' => ['type' => 'none'],
      ]);
      return $parser->parse($text);
    }
  }
  return NULL;
}

/**
 * Implements hook_cron().
 */
function user_prune_cron() {
  $config = \Drupal::config('user_prune.settings');
  $deleteUsers = $config->get('delete_users_on_cron');
  if ($deleteUsers) {
    $users = _user_prune_get_users();
    foreach ($users as $user) {
      \Drupal::entityTypeManager()->getStorage('user')->load($user->uid)->delete();
    }
  }
}

/**
 * Build up or get list of users to prune.
 *
 * @return array
 *   Returns array of users to prune.
 */
function _user_prune_get_users(): array {
  $config = \Drupal::config('user_prune.settings');
  $connection = \Drupal::database();

  $user_prune_time_year = $config->get('year_select');
  $user_prune_time_month = $config->get('month_select');
  $user_prune_time_day = $config->get('day_select');
  $user_never_logged_in = $config->get('user_never_logged_in');
  $user_status = $config->get('user_status');
  $user_with_comment = $config->get('user_with_comment');
  $user_roles = $config->get('user_roles_checkbox');

  // Filter on user created time.
  $logged_in_limit = time() - $user_prune_time_year - $user_prune_time_month - $user_prune_time_day;

  $query = $connection->select('users_field_data', 'u');
  $query->addField('u', 'uid');
  $query->addField('u', 'name', 'Name');
  $query->addField('u', 'created', 'Created');
  $query->addField('u', 'access', 'Lastaccess');
  $query->addField('u', 'login', 'Lastlogin');
  $query->addField('u', 'status', 'Status');

  // Created condition.
  $query->condition('u.created', $logged_in_limit, '<');

  // Make sure not to include power users.
  $query->condition('u.uid', 0, '!=');
  $query->condition('u.uid', 1, '!=');

  // Option to filter only never logged-in users.
  if ($user_never_logged_in) {
    $query->condition('u.access', 0, '=');
    $query->condition('u.login', 0, '=');
  }
  else {
    $query->condition('u.access', $logged_in_limit, '<');
    $query->condition('u.login', $logged_in_limit, '<');
  }

  // Filter based on user roles.
  $selected_roles = array_filter($user_roles);
  foreach ($selected_roles as $role) {
    if ($role) {
      $user_role_subquery = $connection->select('user__roles', 'r');
      $user_role_subquery->addField('r', 'entity_id');
      $user_role_subquery->condition('r.roles_target_id', $role, '=');
      $query->condition('u.uid', $user_role_subquery, 'NOT IN');
    }
  }

  // Option to exclude users with comments.
  if ($user_with_comment == 1) {
    $comment_subquery = $connection->select('comment_field_data', 'c');
    $comment_subquery->addField('c', 'uid');
    $query->condition('u.uid', $comment_subquery, 'NOT IN');
  }

  // Limit based on users active or blocked.
  switch ($user_status) {
    case 'blocked':
      $query->condition('u.status', 0, '=');
      break;

    case 'active':
      $query->condition('u.status', 1, '=');
      break;
  }

  // Order by uid.
  $query->orderBy('u.uid');

  // Limit batches.
  $limit = $config->get('user_number_select');
  if ($limit) {
    $query->range(0, $limit);
  }

  $results = $query->execute();
  return $results->fetchAll();
}
